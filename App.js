import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image
} from 'react-native';
import { PlayersList } from './components/PlayersList'
import { PlayerDetail } from './components/PlayerDetail'
import { StackNavigator } from 'react-navigation';

export default StackNavigator(
  {
    PlayersList: {
      screen: PlayersList
    },
    PlayerDetail: {
      screen: PlayerDetail
    }
  },
  {
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#00BCD4',
      },
      headerTitleStyle: {
        color: '#FFF',
        alignSelf: 'center',
        textAlign: 'center',
        width: '90%'
      },
      headerTintColor: '#FFF'
    }
  }
);