import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    Image,
    TouchableOpacity
} from 'react-native';

export class PlayersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            players: [],
            loading: true
        };
    }

    static navigationOptions = {
        title: 'List Players'
    };

    componentDidMount() {
        return fetch('http://www.mocky.io/v2/5aa722ea2f0000e8048ea463')
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({
                    players: json,
                    loading: false
                }, function () {

                });
            })
            .catch((error) => {
                this.setState({
                    error: error,
                }, function () {

                });
            });
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>
                        Cargando Jugadores..
                    </Text>
                </View>
            )
        }
        return (
            <View>
                <FlatList
                    keyExtractor={(item) => item.id.toString()}
                    data={this.state.players}
                    renderItem={({ item, index }) =>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('PlayerDetail', {player: item})}>
                            <View style={styles.listItem}>
                                <Image source={{ uri: item.thumbnail }} style={{ width: 120, height: 120 }} />
                                <View style={styles.listItemText}>
                                    <Text style={styles.playerName}>
                                        {item.name}
                                    </Text>
                                    <Text style={styles.playerAge}>
                                        Age: {item.age}
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    listItem: {
        flex: 1,
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10
    },
    listItemText: {
        flex: 1,
        justifyContent: 'space-between',
        marginLeft: 5
    },
    playerName: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    playerAge: {
        fontSize: 12
    }
});