import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
} from 'react-native';

export class PlayerDetail extends Component {
    constructor(props) {
        super(props);
    }

    static navigationOptions = {
        title: 'Detail'
    };

    render() {
        let player = this.props.navigation.state.params.player;
        return (
            <View>
                <Image source = {{ uri: player.thumbnail }} style = {styles.backgroundImage} />
                <View style = {styles.infoContainer}>
                    <View style = {styles.card}>
                        <Text style = {styles.playerName}>
                            {player.name}
                        </Text>
                    </View>
                    <View style = {styles.card}>
                        <View style = {{width: '70%', alignSelf: 'center'}}>
                            <View style = {styles.infoLine}>
                                <Text style = {styles.header}>Age:</Text> 
                                <Text>{player.age}</Text>
                            </View>
                            <View style={styles.infoLine}>
                                <Text style={styles.header}>Hair Color:</Text>
                                <Text>{player.hair_color}</Text>
                            </View>
                            <View style={styles.infoLine}>
                                <Text style={styles.header}>Weight:</Text>
                                <Text>{player.weight}</Text>
                            </View>
                            <View style={styles.infoLine}>
                                <Text style={styles.header}>Height:</Text>
                                <Text>{player.height}</Text>
                            </View>
                            <View style={styles.infoLine}>
                                <Text style={styles.header}>Professions:</Text>
                            </View>
                            <View style={styles.professionList}>
                                {
                                    player.professions.map((profession, index) => 
                                        <Text key = {index}> {profession} </Text>
                                    )
                                }
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: '100%',
    height: '65%',
    resizeMode: 'cover'
    },
    card: {
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        backgroundColor: '#FFF'
    },
    playerName: {
        textAlign: 'center',
        fontSize: 25,
        fontWeight: 'bold'
    },
    infoContainer: {
       position: 'absolute',
       top: '55%',
       width: '100%',
       left: 0,
       right: 0
    },
    infoLine: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    header: {
        fontWeight: 'bold'
    },
    professionList: {
        flex: 1,
        alignSelf: 'center'
    }
});