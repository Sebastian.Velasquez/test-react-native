# Listado de Jugadores

## Requisitos
  * NodeJs versión >= 8
  * Android SDK = Api 23

## Ejecución
  * Ejecutar en la terminal `npm install` para instalar dependencias.
  * Ejecutar `npm start` para inciar el servidor de desarrollo.
  * Ejecutar `react-native run android` para ejecutar en un dispositivo android.
